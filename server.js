var express = require('express'),
	app = express();

const PORT = 8080;

app.set('view engine', 'pug');

app.get('/', function(req, res){
	res.render('index');
});
app.use(express.static('./public'));

app.listen(PORT, function(){
	console.log('Listen on port ' + PORT);
});